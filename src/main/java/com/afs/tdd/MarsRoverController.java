package com.afs.tdd;
import com.afs.tdd.Enum.Direction;
import com.afs.tdd.classes.MarsRover;

public class MarsRoverController {
    public MarsRoverController() {}

    public MarsRover turnLeft(MarsRover marsRover) {
        MarsRover newMarsRover = new MarsRover(marsRover);
        if (marsRover.getDirection() == Direction.N) {
            newMarsRover.setDirection(Direction.W);
        }else if (marsRover.getDirection() == Direction.E) {
            newMarsRover.setDirection(Direction.N);
        }else if (marsRover.getDirection() == Direction.S) {
            newMarsRover.setDirection(Direction.E);
        }else if (marsRover.getDirection() == Direction.W) {
            newMarsRover.setDirection(Direction.S);
        }
        return newMarsRover;
    }

    public MarsRover turnRight(MarsRover marsRover) {
        MarsRover newMarsRover = new MarsRover(marsRover);
        if (marsRover.getDirection() == Direction.N) {
            newMarsRover.setDirection(Direction.E);
        }else if (marsRover.getDirection() == Direction.E) {
            newMarsRover.setDirection(Direction.S);
        }else if (marsRover.getDirection() == Direction.S) {
            newMarsRover.setDirection(Direction.W);
        }else if (marsRover.getDirection() == Direction.W) {
            newMarsRover.setDirection(Direction.N);
        }
        return newMarsRover;
    }

    public MarsRover move(MarsRover marsRover) {
        MarsRover newMarsRover = new MarsRover(marsRover);
        if (marsRover.getDirection() == Direction.N) {
            newMarsRover.setY(marsRover.getY() + 1);
        }else if (marsRover.getDirection() == Direction.E) {
            newMarsRover.setX(marsRover.getX() + 1);
        }else if (marsRover.getDirection() == Direction.S) {
            newMarsRover.setY(marsRover.getY() - 1);
        }else if (marsRover.getDirection() == Direction.W) {
            newMarsRover.setX(marsRover.getX() - 1);
        }
        return newMarsRover;
    }

}
