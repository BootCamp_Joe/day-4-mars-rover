package com.afs.tdd;

import com.afs.tdd.Enum.Command;
import com.afs.tdd.classes.MarsRover;

public class SingleCommandProcessor {

    private final MarsRoverController marsRoverController = new MarsRoverController();

    public SingleCommandProcessor(){}

    public MarsRover processSingleCommand(Command singleCommand, MarsRover marsRover) {
        MarsRover updatedMarsRover = new MarsRover(marsRover);
        if (singleCommand == Command.L) {
           updatedMarsRover =  marsRoverController.turnLeft(marsRover);
        }else if (singleCommand == Command.R) {
            updatedMarsRover =  marsRoverController.turnRight(marsRover);
        }else if (singleCommand == Command.M) {
            updatedMarsRover =  marsRoverController.move(marsRover);
        }
        return updatedMarsRover;
    }
}
