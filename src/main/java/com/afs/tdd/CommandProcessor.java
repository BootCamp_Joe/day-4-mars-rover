package com.afs.tdd;

import com.afs.tdd.Enum.Command;
import com.afs.tdd.SingleCommandProcessor;
import com.afs.tdd.classes.MarsRover;

public class CommandProcessor {
    private final SingleCommandProcessor singleCommandProcessor = new SingleCommandProcessor();

    public String processCommand(String command, MarsRover marsRover) {
        MarsRover newMarsRover = new MarsRover(marsRover);
        for(int i=0; i< command.length(); i++){
            newMarsRover = singleCommandProcessor.processSingleCommand(convertCommandFromCharacter(command.charAt(i)), newMarsRover);
        }
        return newMarsRover.toString();
    }

    public Command convertCommandFromCharacter(char command) {
        if (command == 'L') return Command.L;
        if (command == 'R') return Command.R;
        if (command == 'M') return Command.M;
        throw new IllegalArgumentException();
    }


}
