package com.afs.tdd.classes;

import com.afs.tdd.Enum.Direction;

public class MarsRover {
    private int x;
    private int y;
    private Direction direction;

    public MarsRover(int x, int y, Direction direction) {
        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    public MarsRover(MarsRover that) {
        this(that.getX(), that.getY(), that.getDirection());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MarsRover that = (MarsRover) o;
        return this.x == that.x && this.y == that.y && this.direction == that.direction;
    }

    public String toString() {
        return String.format("(%d,%d,%s)", x, y, direction);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }
}
