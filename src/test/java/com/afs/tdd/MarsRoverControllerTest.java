package com.afs.tdd;

import com.afs.tdd.Enum.Direction;
import com.afs.tdd.classes.MarsRover;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MarsRoverControllerTest {

    MarsRoverController marsRoverController = new MarsRoverController();

    @Test
    public void should_face_west_when_turn_left_given_face_north() {
        //given
        MarsRover marsRover = new MarsRover(0, 0, Direction.N);
        MarsRover expected = new MarsRover(0, 0, Direction.W);
        //when
        MarsRover actual = marsRoverController.turnLeft(marsRover);

        //then
        assertEquals(expected, actual);
    }

    @Test
    public void should_face_north_when_turn_left_given_face_east() {
        //given
        MarsRover marsRover = new MarsRover(0, 0, Direction.E);
        MarsRover expected = new MarsRover(0, 0, Direction.N);
        //when
        MarsRover actual = marsRoverController.turnLeft(marsRover);

        //then
        assertEquals(expected, actual);
    }

    @Test
    public void should_face_east_when_turn_left_given_face_south() {
        //given
        MarsRover marsRover = new MarsRover(0, 0, Direction.S);
        MarsRover expected = new MarsRover(0, 0, Direction.E);
        //when
        MarsRover actual = marsRoverController.turnLeft(marsRover);

        //then
        assertEquals(expected, actual);
    }

    @Test
    public void should_face_south_when_turn_left_given_face_west() {
        //given
        MarsRover marsRover = new MarsRover(0, 0, Direction.W);
        MarsRover expected = new MarsRover(0, 0, Direction.S);
        //when
        MarsRover actual = marsRoverController.turnLeft(marsRover);

        //then
        assertEquals(expected, actual);
    }

    @Test
    public void should_face_east_when_turn_right_given_face_north() {
        //given
        MarsRover marsRover = new MarsRover(0, 0, Direction.N);
        MarsRover expected = new MarsRover(0, 0, Direction.E);
        //when
        MarsRover actual = marsRoverController.turnRight(marsRover);

        //then
        assertEquals(expected, actual);
    }

    @Test
    public void should_face_south_when_turn_right_given_face_east() {
        //given
        MarsRover marsRover = new MarsRover(0, 0, Direction.E);
        MarsRover expected = new MarsRover(0, 0, Direction.S);
        //when
        MarsRover actual = marsRoverController.turnRight(marsRover);

        //then
        assertEquals(expected, actual);
    }

    @Test
    public void should_face_west_when_turn_right_given_face_south() {
        //given
        MarsRover marsRover = new MarsRover(0, 0, Direction.S);
        MarsRover expected = new MarsRover(0, 0, Direction.W);
        //when
        MarsRover actual = marsRoverController.turnRight(marsRover);

        //then
        assertEquals(expected, actual);
    }

    @Test
    public void should_face_north_when_turn_right_given_face_west() {
        //given
        MarsRover marsRover = new MarsRover(0, 0, Direction.W);
        MarsRover expected = new MarsRover(0, 0, Direction.N);
        //when
        MarsRover actual = marsRoverController.turnRight(marsRover);

        //then
        assertEquals(expected, actual);
    }

    @Test
    public void should_increase_y_coordinate_by_1_when_move_given_face_north() {
        //given
        MarsRover marsRover = new MarsRover(0, 0, Direction.N);
        MarsRover expected = new MarsRover(0, 1, Direction.N);
        //when
        MarsRover actual = marsRoverController.move(marsRover);

        //then
        assertEquals(expected, actual);
    }

    @Test
    public void should_increase_x_coordinate_by_1_when_move_given_face_east() {
        //given
        MarsRover marsRover = new MarsRover(0, 0, Direction.E);
        MarsRover expected = new MarsRover(1, 0, Direction.E);
        //when
        MarsRover actual = marsRoverController.move(marsRover);

        //then
        assertEquals(expected, actual);
    }

    @Test
    public void should_decrease_y_coordinate_by_1_when_move_given_face_south() {
        //given
        MarsRover marsRover = new MarsRover(0, 0, Direction.S);
        MarsRover expected = new MarsRover(0, -1, Direction.S);
        //when
        MarsRover actual = marsRoverController.move(marsRover);

        //then
        assertEquals(expected, actual);
    }

    @Test
    public void should_decrease_x_coordinate_by_1_when_move_given_face_west() {
        //given
        MarsRover marsRover = new MarsRover(0, 0, Direction.W);
        MarsRover expected = new MarsRover(-1, 0, Direction.W);
        //when
        MarsRover actual = marsRoverController.move(marsRover);

        //then
        assertEquals(expected, actual);
    }
}
