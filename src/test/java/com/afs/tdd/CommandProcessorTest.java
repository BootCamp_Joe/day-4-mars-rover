package com.afs.tdd;

import com.afs.tdd.Enum.Command;
import com.afs.tdd.Enum.Direction;
import com.afs.tdd.classes.MarsRover;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CommandProcessorTest {

    CommandProcessor commandProcessor = new CommandProcessor();

    @Test
    public void should_return_correct_coordinateDirection_when_processCommand_given_command_MLMR_and_location_0_0_N() {
        //given
        MarsRover marsRover = new MarsRover(0, 0, Direction.N);
        String command = "MLMR";
        String expected = "(-1,1,N)";
        //when
        String actual = commandProcessor.processCommand(command, marsRover);

        //then
        assertEquals(expected, actual);
    }

    @Test
    public void should_return_correct_coordinateDirection_when_processCommand_given_command_M_and_location_0_0_N() {
        //given
        MarsRover marsRover = new MarsRover(0, 0, Direction.N);
        String command = "M";
        String expected = "(0,1,N)";
        //when
        String actual = commandProcessor.processCommand(command, marsRover);

        //then
        assertEquals(expected, actual);
    }

    @Test
    public void should_throw_exception_when_processCommand_given_invalid_command() {
        //given
        MarsRover marsRover = new MarsRover(0, 0, Direction.N);
        String command = "MI";
        //when
        assertThrows(IllegalArgumentException.class, () -> {
            commandProcessor.processCommand(command, marsRover);
        });

        //then
    }
}
