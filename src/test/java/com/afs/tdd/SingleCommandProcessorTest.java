package com.afs.tdd;

import com.afs.tdd.Enum.Command;
import com.afs.tdd.Enum.Direction;
import com.afs.tdd.classes.MarsRover;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SingleCommandProcessorTest {

    SingleCommandProcessor singleCommandProcessor = new SingleCommandProcessor();

    @Test
    public void should_face_west_when_turn_left_given_command_left_and_face_north() {
        //given
        MarsRover marsRover = new MarsRover(0, 0, Direction.N);
        MarsRover expected = new MarsRover(0, 0, Direction.W);
        //when
        MarsRover actual = singleCommandProcessor.processSingleCommand(Command.L, marsRover);

        //then
        assertEquals(expected, actual);
    }

    @Test
    public void should_face_east_when_turn_right_given_command_right_and_face_north() {
        //given
        MarsRover marsRover = new MarsRover(0, 0, Direction.N);
        MarsRover expected = new MarsRover(0, 0, Direction.E);
        //when
        MarsRover actual = singleCommandProcessor.processSingleCommand(Command.R, marsRover);

        //then
        assertEquals(expected, actual);
    }

    @Test
    public void should_increase_y_coordinate_by_1_when_move_given_command_move_and_face_north() {
        //given
        MarsRover marsRover = new MarsRover(0, 0, Direction.N);
        MarsRover expected = new MarsRover(0, 1, Direction.N);
        //when
        MarsRover actual = singleCommandProcessor.processSingleCommand(Command.M, marsRover);

        //then
        assertEquals(expected, actual);
    }


}
